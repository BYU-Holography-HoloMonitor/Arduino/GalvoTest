#include <SPI.h>
#include <Wire.h> // I2C

//#include <avr/wdt.h>

/////////////////////////////////////////////////////////////////////////
//
//
//  Galvo Tester for HoloMonitor Scanner Shield
//
//
//  **BEFORE USING, ENSURE THAT THE POLYGON MOTOR IS NOT CONNECTED.**
//  Its control voltage will be swept here at rates faster than it can
//  respond. If it were connected and enabled, the power transistor
//  driving it would quickly overheat from the high current draw.
//
//
//  This software automatically sweeps both DAC outputs at approximately
//  30 Hz, simulating (for a galvanometer mirror) normal runtime usage
//  without an HSYNC/VSYNC signal.
//
//  This software is used to verify the galvo differential signal driver
//  circuitry (both ranging from about +/-5V), the galvo driver, motor,
//  and mirror (to test its integrity), and to verify the voltage scaling
//  circuit for the polygon mirror driver.
//
//  Much of this code is a heavily stripped-down version of the normal
//  runtime Scanner Shield firmware. Some commented-out code for testing
//  miscellaneous things has been left in for your convenience.
//  
/////////////////////////////////////////////////////////////////////////
//
//  Copyright 2016-2018 BYU ElectroHolography Research Group
//  
//  This is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  Send questions to: byuholography@gmail.com
//  Copyright 2016-2017 BYU ElectroHolography Research Group
//  
//  This is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
//  Send questions to: byuholography@gmail.com
//  
/////////////////////////////////////////////////////////////////////////





// interrupts
#define PIN_DSYNC 48 // in, was on 2
#define PIN_PTACH 49 // in, was on 3
#define PIN_HSYNC 18 // in
#define PIN_VSYNC 19 // in

// DVI breakout
#define PIN_HOT_PLUG 22 // out
boolean PIN_HOT_PLUG_state = HIGH;
#define PIN_MIXAMP0 24 // out
#define PIN_MIXAMP1 25 // out
#define PIN_MIXAMP2 23 // out

// PWM & Polygon Control
#define PIN_ESC_PWM 4 // out
#define PIN_POLY_ENABLE 45 // out
boolean PIN_POLY_ENABLE_state = 1; // 0 on, 1 off
#define PIN_POLY_LOCKED 46 // in

// DAC
#define PIN_DAC_SPI_CS 47 // moved from 49 for Input Capture Units // out
#define DAC_CH_GALVO 0
#define DAC_CH_POLY  1
int galvo_value = 0; // keep galvo_value and motor_value as signed ints and not unsigned ints
int motor_value = 0; // this is to allow for signed comparisons to still work (ie, motor_value--; if( motor_value < 0 ) motor_value = 0; )
                     // if these were made unsigned, the above comparison would never work, as motor_value would go from 1 to 0 to 65,535, all of which are positive
// int motor_value_prev = motor_value; // update the motor value on changes

// Serial Communication
// String inputString = "Press 'h' to toggle hotplug pin (currently low)...";        // a string to hold incoming data
// boolean stringComplete = true;  // whether the string is complete

// Auxiliary Circuits for Rev. 26 Sep 2017 board
#define PIN_BUZZER      63  // digital output;  uC PK1; aka analog pin A9
#define PIN_TEMP_SENSOR  5  // analog input A5; uC PF5; aka digital pin 59
int temp_sensor_val;
#define PIN_LED_ACT_SPI 69  // digital output;  uC PK7; aka analog pin A15; GREEN  LED is near DAC
boolean PIN_LED_ACT_SPI_state = 0;
#define PIN_LED_ACT_I2C 14  // digital output;  uC PJ1;                     WHITE  LED is between PLL potentiometers and DVI Breakout port
boolean PIN_LED_ACT_I2C_state = 0;
#define PIN_LED_HSYNC   13  // digital output;  uC PB7; PWM;                ORANGE LED is near AMPL galvo potentiometer
boolean PIN_LED_HSYNC_state = 0;
#define PIN_LED_VSYNC   12  // digital output;  uC PB6; PWM;                BLUE   LED is near galvo output port
boolean PIN_LED_VSYNC_state = 0;



void print_temperature_readable( int temp_val )
{
  // for the TMP36 temperature sensor, Voff: 0.5V, scaling: 10 mV/C, Vout(25C)=750
  // Arduino uses 10-bit values: 0-1023 for 0-5V without changing analogReference() - 4.9 mV/step
  
  // ( analogRead(PIN_TEMP_SENSOR) / 1023 * 5 - 0.5V )/ 0.010V/C = temp in C
  // temp_val * 0.48875 - 50
  
  float temp_report = temp_val * 0.48875855f - 50.0f;
  
  Serial.print( temp_report, 2 );
  Serial.println( 'C' );
}



void init_Scanner_Shield()
{
  // GPIO is by default configured as inputs; but we're just being explicit here

  
  // interruptable pins
  // pinMode( PIN_DSYNC, INPUT );
  // pinMode( PIN_PTACH, INPUT );
  // pinMode( PIN_HSYNC, INPUT );
  // pinMode( PIN_VSYNC, INPUT );

  
  // DVI breakout
  pinMode( PIN_HOT_PLUG,  OUTPUT );
  digitalWrite( PIN_HOT_PLUG, PIN_HOT_PLUG_state );
  // pinMode( PIN_MIXAMP0, INPUT );
  // pinMode( PIN_MIXAMP1, INPUT );
  // pinMode( PIN_MIXAMP2, INPUT );

  
  // configure hardware divider via PORTC
  DDRC  = 0xFF; // set all as outputs
  PORTC = 0x1F; // 0x1F for divide-by-32; 0x3F for divide-by-64
                // PLL works when f_DSYNC = 2 * f_Poly = f_HSYNC / N_CRHL

                
  // PWM & Polygon Control
  // pinMode( PIN_ESC_PWM, INPUT ); // keep as input until needed

  
  //configure the SPI bus
  SPI.begin();
  SPI.setDataMode( SPI_MODE0 );
  SPI.setBitOrder( MSBFIRST );
  pinMode( PIN_DAC_SPI_CS, OUTPUT );
  digitalWrite( PIN_DAC_SPI_CS, HIGH );

  
  // Galvo initialization
  dac_write( DAC_CH_GALVO, galvo_value );

  
  // Polygon Initialization
  pinMode( PIN_POLY_ENABLE, OUTPUT );
  digitalWrite( PIN_POLY_ENABLE, PIN_POLY_ENABLE_state );
  // pinMode( PIN_POLY_LOCKED, INPUT );
  dac_write( DAC_CH_POLY, motor_value );

  // I2C for EDID
  // Wire.begin(); // join i2c bus (address optional for master)
  
  
  // Auxiliary Circuits for Tue 26 Sep 2017 board revision
  pinMode( PIN_BUZZER, OUTPUT );
  pinMode( PIN_LED_ACT_SPI, OUTPUT );
  pinMode( PIN_LED_ACT_I2C, OUTPUT );
  pinMode( PIN_LED_HSYNC, OUTPUT );
  pinMode( PIN_LED_VSYNC, OUTPUT );
  
  // pinMode( PIN_TEMP_SENSOR, INPUT ); no setup needed for analog inputs
  
  // have a general-purpose timer running
  TCNT3 = 0x0000;
  TCCR3A = 0x00;
  TCCR3B = 0x01;
  TCCR3C = 0x00;
  TIMSK3 = 0x00;
}



/*void printCharacterMapping()
{
  // Serial character mapping
  Serial.println( "printing test characters..." );
  int i;
  for( i = 0; i < 256; i++ )
  {
    Serial.print( "0x" );
    Serial.print( i, HEX );
    Serial.print( " '" );
    Serial.print( (char) i );
    Serial.println( "'" );
  }
}*/



void ringBells( unsigned char times )
{
  int i;
  for( i=0; i<times; i++ )
  {
    Serial.print( (char) 7 ); // BEL signal
    delay( 500 );
  }
}



void setup()
{
  // Serial setup
  Serial.begin( 9800 );
  Serial.flush();
  Serial.println();
  Serial.println( "Alive. Firmware compiled/uploaded on " __DATE__ " at " __TIME__ "." );
  Serial.flush();
  Serial.println( "Make sure the polygon motor is NOT connected to the Scanner Shield" );
  Serial.flush();
  Serial.println( "  while using this software (the Galvo Tester)." );
  Serial.flush();
  Serial.println();
  // multiple flushes because the newlines sometimes weren't getting through for some reason
  
  init_Scanner_Shield();

  // printCharacterMapping(); // for determining special characters, like delete or tab
  
  ringBells( 2 );
  
  // buzzer, LED "hello"
  for( int i = 550; i >= 0; i-- )
  {
    digitalWrite( PIN_BUZZER, i & 1 );
    digitalWrite( PIN_LED_ACT_SPI, i>>7 & 1 );
    digitalWrite( PIN_LED_ACT_I2C, i>>4 & 1 );
    digitalWrite( PIN_LED_HSYNC, i>>5 & 1 );
    digitalWrite( PIN_LED_VSYNC, i>>6 & 1 );
    delay(i>>8);
  }
  
  digitalWrite( PIN_BUZZER, LOW );
  digitalWrite( PIN_LED_ACT_SPI, LOW );
  digitalWrite( PIN_LED_ACT_I2C, LOW );
  digitalWrite( PIN_LED_HSYNC, LOW);
  digitalWrite( PIN_LED_VSYNC, LOW );
}



unsigned int timer3_old = 0;
unsigned int ms_counter = 0;

void loop()
{
  // test the galvo
  galvo_value += 4; // a delta of 4 produces an approximately 30 Hz sawtooth command signal
  if( galvo_value > 4095 )
    galvo_value = 0;
  
  motor_value = galvo_value;
  
  dac_write( DAC_CH_GALVO, galvo_value );
  dac_write( DAC_CH_POLY,  motor_value );
  // ramp the B output as well to test the polygon DC bias scaling circuit (op-amps IC10A, IC10B in Scanner Shield schematic)
  
  
  
  // test the temperature sensor once every second
  if( TCNT3 - timer3_old >= 16000 )
  {
    timer3_old = TCNT3;
    
    ms_counter++;
    if( ms_counter == 1000 )
    {
      ms_counter = 0;
      
      temp_sensor_val = analogRead(PIN_TEMP_SENSOR);
      
      PIN_LED_ACT_I2C_state = !PIN_LED_ACT_I2C_state;
      digitalWrite( PIN_LED_ACT_I2C, PIN_LED_ACT_I2C_state );
      
      //Serial.println( temp_sensor_val );
      print_temperature_readable( temp_sensor_val );
    }
  }
  
  
  // test the buzzer
}
  




/**
 * Writes a value to the DAC using the SPI protocol and arduino pins
 * @param address which of two onchip DACs you are writing the value to (0 or 1)
 * @param value the value to be written to the DAC, between 0-1023 inclusive
 */
void dac_write( int address, int value )
{
	//see http://www.datasheetarchive.com/dl/Datasheet-091/DSA0036741.pdf
	// on page 22 for packet details for MCP4822 12-bit DAC
	// takes 16-bit data packets (2 bytes)

  //  send in the address and value via SPI:
  value &= 4095; // rollover values that require more than 12 bits
  value |= (address << 15); //place address is specified part
  value |= (1 << 12); // enable output
  // take the SS pin low to select the chip:
  digitalWrite( PIN_DAC_SPI_CS, LOW );
  // gain set to 2x by default (bit 13 == 0)
  SPI.transfer( highByte( value ) );
  SPI.transfer( lowByte( value ) );
  // take the SS pin high to de-select the chip:
  digitalWrite( PIN_DAC_SPI_CS, HIGH );
}





/*void serialEvent()
{
  while( Serial.available() )
  {
    // get the new byte:
    char inChar = (char) Serial.read();

    switch( inChar )
    {
      case 'h':
      pin_HOT_PLUG_state = !pin_HOT_PLUG_state;
      digitalWrite( pin_HOT_PLUG, pin_HOT_PLUG_state );
      Serial.println( "hot_plug = " + String( pin_HOT_PLUG_state ) );
      break;
      
      case 'm':
      pin_POLY_ENABLE_state = !pin_POLY_ENABLE_state;
      digitalWrite( pin_POLY_ENABLE, pin_POLY_ENABLE_state );
      Serial.println( "motor_enable = " + String( pin_POLY_ENABLE_state ) );
      break;
      
      case '4':
      case '6':
      case '2':
      case '8':
        switch( inChar )
        {
          case '4':
          motor_value--;
          if( motor_value < 0 )
            motor_value = 0;
          Serial.println( "motor_value = " + String( motor_value ) );
          break;
          
          case '6':
          motor_value++;
          if( motor_value > 4095 )
            motor_value = 4095;
          Serial.println( "motor_value = " + String( motor_value ) );
          break;
          
          case '2':
          motor_value-=20;
          if( motor_value < 0 )
            motor_value = 0;
          Serial.println( "motor_value = " + String( motor_value ) );
          break;
          
          case '8':
          motor_value+=20;
          if( motor_value > 4095 )
            motor_value = 4095;
          Serial.println( "motor_value = " + String( motor_value ) );
          break;
        }
        
        dac_write( DAC_CH_POLY, motor_value );
      break;
    }

    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    // if( inChar == '\n' )
    // {
    //   stringComplete = true;
    // }
    // else
    // {
    //   // add it to the inputString:
    //   inputString += inChar;
    // }
  }
}*/



















