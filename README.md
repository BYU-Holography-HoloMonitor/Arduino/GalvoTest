# Arduino Galvanometer Testing

This repostiory contains an Arduino sketch (firmware) for tesing galvanometer control with the ScannerShield. It makes use of the SPI DAC onboard the Shield and an external Galvo Driver. Derived from the original EDID programmer code (Arduino/EDID).

Any questions can be directed to byuholography@gmail.com

# License

All files in this repository are Copyright 2016 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
